package com.zahraakhtaraziz.setas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class YourProfile extends AppCompatActivity {
    ImageView home;
    EditText fname,lname,cnic,province,city,phone,minstitution,iinstitution,mpercent,ipercent,mboard,iboard,program;
    String strmboard,striboard,strfname,strlname,strcnic,strprovince,strcity,strphone,strmpercent,stripercent,strminstitution,striinstitution,strprogram;
    FirebaseAuth mAuth;
    FirebaseDatabase database;
    DatabaseReference reference;
    Button update;
    ImageView settingsbtn;
    TextView sprofile,sreset,slogout;
    BottomSheetDialog bottomSheetDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_profile);

        mAuth=FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        reference = database.getReference("Users");

        home=findViewById(R.id.home);
        update=findViewById(R.id.update);
        settingsbtn=findViewById(R.id.settingsbtn);

        fname=findViewById(R.id.fname);
        lname=findViewById(R.id.lname);
        cnic=findViewById(R.id.cnic);
        province=findViewById(R.id.province);
        city=findViewById(R.id.city);
        phone=findViewById(R.id.phone);
        mpercent=findViewById(R.id.mpercent);
        ipercent=findViewById(R.id.ipercent);
        mboard=findViewById(R.id.mboard);
        iboard=findViewById(R.id.iboard);
        minstitution=findViewById(R.id.minstitution);
        iinstitution=findViewById(R.id.iinstitution);
        program=findViewById(R.id.program);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String userid = user.getUid();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Users");
        ref.child(userid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String fname1 = dataSnapshot.child("fname").getValue().toString();
                String lname1 = dataSnapshot.child("lname").getValue().toString();
                String cnic1 = dataSnapshot.child("cnic").getValue().toString();
                String province1 = dataSnapshot.child("province").getValue().toString();
                String city1 = dataSnapshot.child("city").getValue().toString();
                String phone1 = dataSnapshot.child("phone").getValue().toString();
                String minstitution1 = dataSnapshot.child("minstitution").getValue().toString();
                String mboard1 = dataSnapshot.child("mboard").getValue().toString();
                String mpercent1 = dataSnapshot.child("mpercent").getValue().toString();
                String iinstitution1 = dataSnapshot.child("iinstitution").getValue().toString();
                String iboard1 = dataSnapshot.child("iboard").getValue().toString();
                String ipercent1 = dataSnapshot.child("ipercent").getValue().toString();
                String program1 = dataSnapshot.child("program").getValue().toString();

                fname.setText(fname1);
                lname.setText(lname1);
                cnic.setText(cnic1);
                province.setText(province1);
                city.setText(city1);
                phone.setText(phone1);
                minstitution.setText(minstitution1);
                mboard.setText(mboard1);
                mpercent.setText(mpercent1);
                iinstitution.setText(iinstitution1);
                iboard.setText(iboard1);
                ipercent.setText(ipercent1);
                program.setText(program1);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        settingsbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog = new BottomSheetDialog(YourProfile.this);
                bottomSheetDialog.setContentView(R.layout.settings);

                sprofile=bottomSheetDialog.findViewById(R.id.sprofile);
                sreset=bottomSheetDialog.findViewById(R.id.sreset);
                slogout=bottomSheetDialog.findViewById(R.id.slogout);

                bottomSheetDialog.setCanceledOnTouchOutside(false);
                bottomSheetDialog.show();

                sprofile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(YourProfile.this,YourProfile.class));
                    }
                });

                sreset.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(YourProfile.this,ResetPassword.class));
                    }
                });

                slogout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(YourProfile.this,MainActivity.class));
                    }
                });

            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(YourProfile.this, MainPage.class));
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strfname=fname.getText().toString();
                strlname=lname.getText().toString();
                strcnic=cnic.getText().toString();
                strprovince=province.getText().toString();
                strcity=city.getText().toString();
                strphone=phone.getText().toString();
                strminstitution=minstitution.getText().toString();
                strmpercent=mpercent.getText().toString();
                striinstitution=iinstitution.getText().toString();
                strprogram=program.getText().toString();
                stripercent=ipercent.getText().toString();
                strmboard=mboard.getText().toString();
                striboard=iboard.getText().toString();

                database = FirebaseDatabase.getInstance();
                reference = database.getReference("Users");
                User myInfo= new User(strfname,strlname,strcnic,strprovince,strcity,strphone,strminstitution,strmboard,strmpercent,striinstitution,striboard,stripercent,strprogram);
                reference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(myInfo);
                startActivity(new Intent(YourProfile.this, MainPage.class));
            }
        });
    }
 
}