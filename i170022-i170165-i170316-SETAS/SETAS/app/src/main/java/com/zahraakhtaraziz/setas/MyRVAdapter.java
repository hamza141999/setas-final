package com.zahraakhtaraziz.setas;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyRVAdapter extends RecyclerView.Adapter <MyRVAdapter.MyViewHolder> implements Filterable {
    List<University> ls, lscopy;
    FirebaseDatabase database =FirebaseDatabase.getInstance();
    DatabaseReference reference, ref;


    Context c;


    int check = 0;


    public MyRVAdapter(List<University> ls, Context c) {
        this.c = c;
        this.ls = ls;
        lscopy = new ArrayList<>(ls);
        System.out.println("CONSTRUCTOR"+lscopy.size());

    }


    @NonNull
    @Override
    public MyRVAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemrow = LayoutInflater.from(c).inflate(R.layout.row, parent, false);
        return new MyViewHolder(itemrow);


    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(@NonNull final MyRVAdapter.MyViewHolder holder, final int position) {
        String context = c.toString();
        System.out.println(context);

        holder.name.setText(ls.get(position).Name);
        holder.city.setText(ls.get(position).City);
        holder.address.setText(ls.get(position).Address);

        holder.rowww.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

//                System.out.println("RV"+uid);


//                DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Universities");
//
//
//                database.getReference().child("Universities").addValueEventListener(new ValueEventListener() {
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
////                            System.out.println("refereee2 "+snapshot.child("phone").getValue().toString());
////                            for (DataSnapshot bioSnapshot: snapshot.child("bio").getChildren()) {
////                                System.out.println("refereee1");
//
//                            if (snapshot.child("uniName").getValue().toString().equals(ls.get(position).getName()))
//                            {
                System.out.println("REFEREEE " + ls.get(position).getName());

                c.startActivity(new Intent(c, UniProfile.class).putExtra("uni", ls.get(position).getName()));

//                            }
//
////                            }
//                        }
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                    }
//                });
//

            }
        });

        }

    @Override
    public int getItemCount() {
        return ls.size();
    }

    public Filter getFilter() {
        return listFilter;
    }

    private Filter listFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<University> filteredList= new ArrayList<>();
            if (constraint==null || constraint.length()==0)
            {
                filteredList.addAll(lscopy);
            }
            else
            {
                String filterpattern=constraint.toString().toLowerCase().trim();
                System.out.println("RVRVRV "+filterpattern);
                for (University item : lscopy)
                {
                    if (item.getName().toLowerCase().contains(filterpattern))
                    {
                        System.out.println("ADDINGGG "+item);
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results=new FilterResults();
            results.values=filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

            ls.clear();
            System.out.println("PUBLISHH"+filterResults);
            ls.addAll((List)filterResults.values);
            notifyDataSetChanged();
        }
    };



    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name,city,address;
        LinearLayout rowww;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            city=itemView.findViewById(R.id.city);
            address=itemView.findViewById(R.id.address);
            rowww=itemView.findViewById(R.id.rowww);
            database = FirebaseDatabase.getInstance();
        }
    }
    public void removeItem(int position) {
        ls.remove(position);
        notifyItemRemoved(position);
        // Add whatever you want to do when removing an Item
    }
}
