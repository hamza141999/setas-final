package com.zahraakhtaraziz.setas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Search extends AppCompatActivity {
    ImageView home,profile;
    ArrayList<University> ls,searchList;

    RecyclerView rv;
    RecyclerView.LayoutManager lm;
    MyRVAdapter adapter;
    SearchView search;
    ArrayList<String> names=new ArrayList<String>();
    Editable text;
    FirebaseAuth mAuth;
    FirebaseDatabase database;
    DatabaseReference reference;
    ArrayList<String>uni=new ArrayList<>();
    ImageView settingsbtn;
    TextView sprofile,sreset,slogout;
    BottomSheetDialog bottomSheetDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        settingsbtn=findViewById(R.id.settingsbtn);
        home=findViewById(R.id.home);
        profile=findViewById(R.id.profile);
        ls=new ArrayList<>();
        searchList=new ArrayList<>();
        rv=findViewById(R.id.rv);
        search=(SearchView) findViewById(R.id.search);




        settingsbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog = new BottomSheetDialog(Search.this);
                bottomSheetDialog.setContentView(R.layout.settings);

                sprofile=bottomSheetDialog.findViewById(R.id.sprofile);
                sreset=bottomSheetDialog.findViewById(R.id.sreset);
                slogout=bottomSheetDialog.findViewById(R.id.slogout);

                bottomSheetDialog.setCanceledOnTouchOutside(false);
                bottomSheetDialog.show();

                sprofile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(Search.this,YourProfile.class));
                    }
                });

                sreset.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(Search.this,ResetPassword.class));
                    }
                });

                slogout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(Search.this,MainActivity.class));
                    }
                });

            }
        });


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Search.this, MainPage.class));
            }
        });
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Search.this, YourProfile.class));
            }
        });




        uni.add("uni_fast");
        uni.add("uni_nust");
        uni.add("uni_habib");
        uni.add("uni_uop");
        uni.add("uni_comsats");
        uni.add("uni_islamic");
        uni.add("uni_giki");
        uni.add("uni_buet");
        uni.add("uni_buitems");


        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Universities");
        rv=findViewById(R.id.rv);
        lm= new LinearLayoutManager(this);
        rv.setLayoutManager(lm);





        for (int i =0;i<uni.size();i++) {
            ref.child(uni.get(i)).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String name = dataSnapshot.child("uniName").getValue().toString();
                    String email = dataSnapshot.child("uniEmail").getValue().toString();
                    String city = dataSnapshot.child("city").getValue().toString();
                    System.out.println("FETCH" + name + " " + email + " " + city);

                    ls.add(new University(name, email, city));
                    adapter = new MyRVAdapter(ls,Search.this);
                    rv.setAdapter(adapter);
                    adapter.notifyDataSetChanged();


                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
            System.out.println("LENNN "+ls.size());

        }

//        adapter.notifyDataSetChanged();



//
//        ls.add(new University("FAST NU","Islamabad","A.K Brohi Road H-11"));
//        ls.add(new University("FAST NU","Lahore","channo wale stand ke sath"));
//        ls.add(new University("NUST","Islamabad","A.K Brohi Road H-11"));
//        ls.add(new University("COMSATS","Islamabad","Kisi Khud mai"));
//        ls.add(new University("Bahria University","Islamabad","E-9 Campus"));


//        System.out.println("HEREEE"+Search_adapter);
//


        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (newText!=null) {
                    System.out.println("HERE!...." + newText);
                    adapter.getFilter().filter(newText);
                    adapter.notifyDataSetChanged();
                }

                return false;
            }
        });

//        adapter.getFilter().filter(search.getQuery());
//        rv.setAdapter(adapter);

//        adapter.notifyDataSetChanged();
    }



}