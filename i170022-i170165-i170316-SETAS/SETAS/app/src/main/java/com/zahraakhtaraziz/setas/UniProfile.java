package com.zahraakhtaraziz.setas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class UniProfile extends AppCompatActivity {

    ImageView home, profile;
    TextView name,province,city,sector,fee,rank,contact,email,zip;
    FirebaseDatabase database;
    ImageView settingsbtn;
    TextView sprofile,sreset,slogout;
    BottomSheetDialog bottomSheetDialog;
    Button website,feestructure,apply;
    String strwebsite,strfeestructure,strapply;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uni_profile);

        settingsbtn=findViewById(R.id.settingsbtn);
        home=findViewById(R.id.home);
        profile=findViewById(R.id.profile);
        name=findViewById(R.id.name);
        province=findViewById(R.id.province);
        city=findViewById(R.id.city);
        sector=findViewById(R.id.sector);
        fee=findViewById(R.id.fee);
        rank=findViewById(R.id.rank);
        contact=findViewById(R.id.number);
        email=findViewById(R.id.email);
        zip=findViewById(R.id.zipcode);
        website=findViewById(R.id.website);
        feestructure=findViewById(R.id.feestructure);
        apply=findViewById(R.id.apply);
        database=FirebaseDatabase.getInstance();

        settingsbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog = new BottomSheetDialog(UniProfile.this);
                bottomSheetDialog.setContentView(R.layout.settings);

                sprofile=bottomSheetDialog.findViewById(R.id.sprofile);
                sreset=bottomSheetDialog.findViewById(R.id.sreset);
                slogout=bottomSheetDialog.findViewById(R.id.slogout);

                bottomSheetDialog.setCanceledOnTouchOutside(false);
                bottomSheetDialog.show();

                sprofile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(UniProfile.this,YourProfile.class));
                    }
                });

                sreset.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(UniProfile.this,ResetPassword.class));
                    }
                });

                slogout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(UniProfile.this,MainActivity.class));
                    }
                });

            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(UniProfile.this, MainPage.class));
            }
        });
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(UniProfile.this, YourProfile.class));
            }
        });

        Intent intent=getIntent();
        final String UniName=intent.getStringExtra("uni");
        System.out.println("Namee "+UniName);

        database.getReference().child("Universities").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    if (snapshot.child("uniName").getValue().toString().equals(UniName))
                    {
//                            {

                        System.out.println("INNN "+snapshot.child("uniName").getValue());

                        String name1 = snapshot.child("uniName").getValue().toString();
                        String province1 = snapshot.child("province").getValue().toString();
                        String city1 = snapshot.child("city").getValue().toString();
                        String sector1 = snapshot.child("sector").getValue().toString();
                        String fee1 = snapshot.child("fee").getValue().toString();
                        String rank1 = snapshot.child("rank").getValue().toString();
                        String phone1 = snapshot.child("phone").getValue().toString();
                        String email1 = snapshot.child("uniEmail").getValue().toString();
                        String zip1 = snapshot.child("zip").getValue().toString();
                         strapply = snapshot.child("website_apply").getValue().toString();
                         strfeestructure = snapshot.child("website_fee").getValue().toString();
                         strwebsite = snapshot.child("website").getValue().toString();

                        name.setText(name1);
                        province.setText(province1);
                        city.setText(city1);
                        sector.setText(sector1);
                        fee.setText(fee1);
                        rank.setText(rank1);
                        contact.setText(phone1);
                        email.setText(email1);
                        zip.setText(zip1);

                    }
                }
            }


            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                myWebLink.setData(Uri.parse(strwebsite));
                startActivity(myWebLink);
            }
        });

        feestructure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                myWebLink.setData(Uri.parse(strfeestructure));
                startActivity(myWebLink);
            }
        });

        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                myWebLink.setData(Uri.parse(strapply));
                startActivity(myWebLink);
            }
        });

    }


}