package com.zahraakhtaraziz.setas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.smarteist.autoimageslider.DefaultSliderView;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderLayout;
import com.smarteist.autoimageslider.SliderView;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class MainPage extends AppCompatActivity {
    ImageView profile, home, menu;
    ImageView settingsbtn;
    TextView sprofile,sreset,slogout;
    BottomSheetDialog bottomSheetDialog;
    SliderLayout sliderLayout;
    NavigationView navigationView;
    View headerView;
    String strprovince;
    ArrayList<String> uni=new ArrayList<>();
    TextView navSearchUni,navProvince,navtests,navUniProfile,navMyprofile,navLogout;
    DrawerLayout drwr;
    String flag="";
//    private Toolbar toolbar;
//    private NavigationView nvDrawer;
//    MenuItem entrytest, manageprofil, searchuni, recommenduni, register, uni, account, settings, logout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);

        profile = findViewById(R.id.myprofile);
        home = findViewById(R.id.home);
        drwr=findViewById(R.id.drwr);
        menu=findViewById(R.id.menu);
        sliderLayout = findViewById(R.id.imageSlider);
        sliderLayout.setIndicatorAnimation(IndicatorAnimations.FILL);
        sliderLayout.setScrollTimeInSec(1);
        setSliderViews();
        navigationView = (NavigationView) findViewById(R.id.navigation_id);
        headerView = navigationView.getHeaderView(0);
        navSearchUni = (TextView) headerView.findViewById(R.id.search);
        settingsbtn=findViewById(R.id.settingsbtn);

        navProvince = (TextView) headerView.findViewById(R.id.searchbyprovince);

        navtests = (TextView) headerView.findViewById(R.id.sampletest);
        navMyprofile = (TextView) headerView.findViewById(R.id.myprofile);
        navLogout = (TextView) headerView.findViewById(R.id.logout);

        Intent check=getIntent();
        flag=check.getStringExtra("flag");

        settingsbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog = new BottomSheetDialog(MainPage.this);
                bottomSheetDialog.setContentView(R.layout.settings);

                sprofile=bottomSheetDialog.findViewById(R.id.sprofile);
                sreset=bottomSheetDialog.findViewById(R.id.sreset);
                slogout=bottomSheetDialog.findViewById(R.id.slogout);

                bottomSheetDialog.setCanceledOnTouchOutside(false);
                bottomSheetDialog.show();

                sprofile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(MainPage.this,YourProfile.class));
                    }
                });

                sreset.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(MainPage.this,ResetPassword.class));
                    }
                });

                slogout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(MainPage.this,MainActivity.class));
                    }
                });

            }
        });

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String userid = user.getUid();
        DatabaseReference ref1 = FirebaseDatabase.getInstance().getReference("Users");
        ref1.child(userid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                strprovince = dataSnapshot.child("province").getValue().toString();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        navSearchUni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainPage.this,Search.class));
            }
        });

        navProvince.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainPage.this,ProvinceUniversity.class);
                intent.putExtra("province",strprovince);
                startActivity(intent);
            }
        });

        navtests.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainPage.this,sampleTests.class));
            }
        });

        navMyprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainPage.this, YourProfile.class));
            }
        });
        navLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainPage.this,MainActivity.class));
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                nav_email.setText(StaticClass.user.email.toString());
//                nav_name.setText(StaticClass.user.name.toString());
//                nav_img.setImageURI(Uri.parse(StaticClass.user.img));
//                Toast.makeText(getApplicationContext(),"Menu Clicked",Toast.LENGTH_SHORT).show();

                if(drwr.isDrawerOpen(Gravity.RIGHT))
                {

                    drwr.closeDrawer(Gravity.RIGHT);

                }
                else
                {

                    drwr.openDrawer(Gravity.RIGHT);
                }
            }

        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainPage.this, YourProfile.class));
            }
        });
//
//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        uni.add("uni_fast");
        uni.add("uni_nust");
        uni.add("uni_habib");
        uni.add("uni_uop");
        uni.add("uni_giki");
        uni.add("uni_buet");
        uni.add("uni_buitems");

        System.out.println("FLAGGGGG: "+flag);
        if(flag!=null) {
            if (flag.equals("1")) {
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Universities");
                for (int i = 0; i < uni.size(); i++) {
                    ref.child(uni.get(i)).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String testDate = dataSnapshot.child("testDate").getValue().toString();
                            String uniName = dataSnapshot.child("uniName").getValue().toString();
                            long daystest = calculateDays("15/12/2020", testDate);
                            System.out.println("DAYSSSSSSSSSSSSSSSSS:" + daystest + uniName + testDate);
                            if (daystest == 10) {
                                notificationDialog(uniName, testDate, Long.toString(daystest));
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                }
            }
        }

    }
    private void setSliderViews() {
        for (int i = 1; i <= 6; i++) {
            DefaultSliderView sliderView = new DefaultSliderView(this);

            switch (i){
                case 1:
                    sliderView.setImageDrawable(R.drawable.img1);
                    break;
                case 2:
                    sliderView.setImageDrawable(R.drawable.img2);
                    break;
                case 3:
                    sliderView.setImageDrawable(R.drawable.img3);
                    break;
                case 4:
                    sliderView.setImageDrawable(R.drawable.img4);
                    break;
                case 5:
                    sliderView.setImageDrawable(R.drawable.img5);
                    break;
                case 6:
                    sliderView.setImageDrawable(R.drawable.img6);
                    break;

            }
            sliderView.setImageScaleType(ImageView.ScaleType.CENTER_CROP);
//            sliderView.setDescription("setDescription " +(i+1));
            final int finalI = i;
            sliderView.setOnSliderClickListener(new SliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(SliderView sliderView) {
                    Toast.makeText(MainPage.this, "This slider" + (finalI + 1), Toast.LENGTH_SHORT).show();
                }
            });
            sliderLayout.addSliderView(sliderView);
        }
    }


//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.drawer_view, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//
//        if (id == R.id.entrytest) {
//            startActivity(new Intent(this, sampleTests.class));
//            return true;
//        }
//        if (id == R.id.manageprofile) {
//            startActivity(new Intent(this, YourProfile.class));
//            return true;
//        }
//        if (id == R.id.searchuni) {
//            startActivity(new Intent(this, Search.class));
//            return true;
//        }
//        if (id == R.id.recommenduni) {
//            startActivity(new Intent(this, UniRecommendation.class));
//            return true;
//        }
//        if (id == R.id.register) {
//            startActivity(new Intent(this, RegistrationForm.class));
//            return true;
//        }
//        if (id == R.id.uni) {
//            startActivity(new Intent(this, UniProfile.class));
//            return true;
//        }
//        if (id == R.id.account) {
//            //startActivity(new Intent(this,UniRecommendation.class));
//            return true;
//        }
//        if (id == R.id.settings) {
//            //startActivity(new Intent(this,UniRecommendation.class));
//            return true;
//        }
//        if (id == R.id.logout) {
//            startActivity(new Intent(this, MainActivity.class));
//            return true;
//        }
//
//
//        return super.onOptionsItemSelected(item);
//    }

    public long calculateDays(String date1, String date2) {
        long days = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDate d1 = LocalDate.parse(date1, formatter);
            LocalDate d2 = LocalDate.parse(date2, formatter);
            days = ChronoUnit.DAYS.between(d1, d2);
        }
        return days;
    }

    private void notificationDialog(String uni, String unidate,String days) {
        String display= "The admission test for " + uni+" is on " +unidate+". Only " + days + " days left. Tap here to prepare for the test!";
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "C_id";//must me unique within the app
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            @SuppressLint("WrongConstant")
            NotificationChannel notificationChannel = new NotificationChannel(
                    NOTIFICATION_CHANNEL_ID,
                    "My Notifications",
                    NotificationManager.IMPORTANCE_MAX
            );
            // Configure the notification channel.
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        Intent intent= new Intent(getApplicationContext(),sampleTests.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent=PendingIntent.getActivity(getApplicationContext(),1,intent,PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        notificationBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.logo)
                .setContentTitle(days+" days left!")
                .setContentText(display)
                .setContentInfo("content")
                .setContentIntent(pendingIntent);
        notificationManager.notify(1, notificationBuilder
                .setStyle(new NotificationCompat.BigTextStyle().bigText(display))
                .build());
    }

}