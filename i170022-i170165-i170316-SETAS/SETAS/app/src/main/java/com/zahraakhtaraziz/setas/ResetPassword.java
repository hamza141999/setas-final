package com.zahraakhtaraziz.setas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;

public class ResetPassword extends AppCompatActivity {
    Button resetpw;
    FirebaseAuth mAuth;
    EditText resetemail;
    String stremail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        mAuth=FirebaseAuth.getInstance();
        resetpw=findViewById(R.id.resetpw);
        resetemail=findViewById(R.id.resetemail);

        resetpw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stremail=resetemail.getText().toString();
                mAuth.sendPasswordResetEmail(stremail).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(ResetPassword.this,"Password reset link has been sent to your email!",Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(ResetPassword.this,"Failed to send password reset link"+e.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                });
                startActivity(new Intent(ResetPassword.this,Login.class));
            }
        });
    }
}