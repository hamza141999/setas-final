package com.zahraakhtaraziz.setas;

public class University {
    public String Name;
    public String City;
    public String Address;
    public String Province;

    public String getName() {
        return Name;
    }
    public University()
    {
        this.Name="";
        this.City="";
        this.Address="";
    }

    public University(String name, String city, String address) {
        this.Name = name;
        City = city;
        Address = address;
    }

    public University(String name, String city, String address,String province) {
        this.Name = name;
        City = city;
        Address = address;
        this.Province=province;
    }

    public String getProvince() {
        return Province;
    }

    public void setProvince(String province) {
        Province = province;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }
}
