package com.zahraakhtaraziz.setas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ProvinceUniversity extends AppCompatActivity {
    ImageView home,profile;
    ArrayList<University> ls,searchList;
    String strprovince;
    EditText myprovince;
    ImageView settingsbtn;
    TextView sprofile,sreset,slogout;
    BottomSheetDialog bottomSheetDialog;

    RecyclerView rv;
    RecyclerView.LayoutManager lm;
    MyRVAdapter adapter;
    SearchView search;
    ArrayList<String> names=new ArrayList<String>();
    Editable text;
    FirebaseAuth mAuth;
    FirebaseDatabase database;
    DatabaseReference reference;
    ArrayList<String>uni=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_province_university);

        home=findViewById(R.id.home);
        settingsbtn=findViewById(R.id.settingsbtn);
        profile=findViewById(R.id.profile);
        ls=new ArrayList<>();
        searchList=new ArrayList<>();
        rv=findViewById(R.id.rv);
        search=(SearchView) findViewById(R.id.search);
        myprovince=findViewById(R.id.myprovince);
        rv=findViewById(R.id.rv);


        Intent intent= getIntent();
        strprovince=intent.getStringExtra("province");
        myprovince.setText(strprovince);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProvinceUniversity.this, MainPage.class));
            }
        });
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProvinceUniversity.this, YourProfile.class));
            }
        });

        settingsbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog = new BottomSheetDialog(ProvinceUniversity.this);
                bottomSheetDialog.setContentView(R.layout.settings);

                sprofile=bottomSheetDialog.findViewById(R.id.sprofile);
                sreset=bottomSheetDialog.findViewById(R.id.sreset);
                slogout=bottomSheetDialog.findViewById(R.id.slogout);

                bottomSheetDialog.setCanceledOnTouchOutside(false);
                bottomSheetDialog.show();

                sprofile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(ProvinceUniversity.this,YourProfile.class));
                    }
                });

                sreset.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(ProvinceUniversity.this,ResetPassword.class));
                    }
                });

                slogout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(ProvinceUniversity.this,MainActivity.class));
                    }
                });

            }
        });

        uni.add("uni_fast");
        uni.add("uni_nust");
        uni.add("uni_comsats");
        uni.add("uni_islamic");
        uni.add("uni_habib");
        uni.add("uni_uop");
        uni.add("uni_giki");
        uni.add("uni_buet");
        uni.add("uni_buitems");


        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Universities");

        for (int i =0;i<uni.size();i++) {
            ref.child(uni.get(i)).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String name = dataSnapshot.child("uniName").getValue().toString();
                    String email = dataSnapshot.child("uniEmail").getValue().toString();
                    String city = dataSnapshot.child("city").getValue().toString();
                    String province = dataSnapshot.child("province").getValue().toString();

                    System.out.println("FETCH" + name + " " + email + " " + city+ " "+ province+ " "+ strprovince);

                    if(province.equalsIgnoreCase(strprovince)) {
                        System.out.println("HEREE!");
                        ls.add(new University(name, email, city, province));
                        lm= new LinearLayoutManager(ProvinceUniversity.this);
                        rv.setLayoutManager(lm);
                        adapter = new MyRVAdapter(ls,ProvinceUniversity.this);
                        rv.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
            System.out.println("LENNN "+ls.size());

        }





//
//        ls.add(new University("FAST NU","Islamabad","A.K Brohi Road H-11"));
//        ls.add(new University("FAST NU","Lahore","channo wale stand ke sath"));
//        ls.add(new University("NUST","Islamabad","A.K Brohi Road H-11"));
//        ls.add(new University("COMSATS","Islamabad","Kisi Khud mai"));
//        ls.add(new University("Bahria University","Islamabad","E-9 Campus"));


//        System.out.println("HEREEE"+Search_adapter);
//


        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (newText!=null) {
                    System.out.println("HEREPRO!...." + newText);
                    adapter.getFilter().filter(newText);
                    adapter.notifyDataSetChanged();
                }
                return false;
            }
        });

//        adapter.getFilter().filter(search.getQuery());
//        rv.setAdapter(adapter);


    }
}