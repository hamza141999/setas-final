package com.zahraakhtaraziz.setas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.navigation.NavigationView;

public class sampleTests extends AppCompatActivity {

    ImageView home,profile;
    Button nat1,nat2,gmat,netcs1,netcs2,neteng1,neteng2,netmg1,netmg2,ibacs1,ibacs2,ibacs3,ibabba1,ibabba2,giki1,giki2,giki3,giki4,giki5;
    ImageView settingsbtn;
    TextView sprofile,sreset,slogout;
    BottomSheetDialog bottomSheetDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_tests);

        settingsbtn=findViewById(R.id.settingsbtn);
        home=findViewById(R.id.home);
        profile=findViewById(R.id.profile);
        nat1=findViewById(R.id.nat1);
        nat2=findViewById(R.id.nat2);
        gmat=findViewById(R.id.gmat);
        netcs1=findViewById(R.id.netcs1);
        netcs2=findViewById(R.id.netcs2);
        neteng1=findViewById(R.id.neteng1);
        neteng2=findViewById(R.id.neteng2);
        netmg1=findViewById(R.id.netmg1);
        netmg2=findViewById(R.id.netmg2);
        ibacs1=findViewById(R.id.ibacs1);
        ibacs2=findViewById(R.id.ibacs2);
        ibacs3=findViewById(R.id.ibacs3);
        ibabba1=findViewById(R.id.ibabba1);
        ibabba2=findViewById(R.id.ibabba2);
        giki1=findViewById(R.id.giki1);
        giki2=findViewById(R.id.giki2);
        giki3=findViewById(R.id.giki3);
        giki4=findViewById(R.id.giki4);
        giki5=findViewById(R.id.giki5);


        settingsbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog = new BottomSheetDialog(sampleTests.this);
                bottomSheetDialog.setContentView(R.layout.settings);

                sprofile=bottomSheetDialog.findViewById(R.id.sprofile);
                sreset=bottomSheetDialog.findViewById(R.id.sreset);
                slogout=bottomSheetDialog.findViewById(R.id.slogout);

                bottomSheetDialog.setCanceledOnTouchOutside(false);
                bottomSheetDialog.show();

                sprofile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(sampleTests.this,YourProfile.class));
                    }
                });

                sreset.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(sampleTests.this,ResetPassword.class));
                    }
                });

                slogout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(sampleTests.this,MainActivity.class));
                    }
                });

            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(sampleTests.this, MainPage.class));
            }
        });
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(sampleTests.this, YourProfile.class));
            }
        });

        nat1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                myWebLink.setData(Uri.parse("https://firebasestorage.googleapis.com/v0/b/setas-18760.appspot.com/o/NAT%2Fnat-1a-sample-test-01%20(1).pdf?alt=media&token=41a91ac5-7d9f-43b5-b287-52d4611755c8"));
                startActivity(myWebLink);
            }
        });

        nat2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                myWebLink.setData(Uri.parse("https://firebasestorage.googleapis.com/v0/b/setas-18760.appspot.com/o/NAT%2Fnat-ia-sample-test-02%20(1).pdf?alt=media&token=8b35c358-448d-4467-b442-f7da7b5a4354"));
                startActivity(myWebLink);
            }
        });

        gmat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                myWebLink.setData(Uri.parse("https://firebasestorage.googleapis.com/v0/b/setas-18760.appspot.com/o/GMAT%2FGMATSP.pdf?alt=media&token=530f0376-8c85-4857-9356-2750f535c658"));
                startActivity(myWebLink);
            }
        });

        netcs1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                myWebLink.setData(Uri.parse("https://firebasestorage.googleapis.com/v0/b/setas-18760.appspot.com/o/NET%2Fnust-computer-science-sample-paper-01.pdf?alt=media&token=206a4113-76b8-40dc-93f3-fc54663e8105"));
                startActivity(myWebLink);
            }
        });

        netcs2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                myWebLink.setData(Uri.parse("https://firebasestorage.googleapis.com/v0/b/setas-18760.appspot.com/o/NET%2Fnust-computer-science-sample-paper-02.pdf?alt=media&token=cd166679-87ac-4785-ae62-7032dab50002"));
                startActivity(myWebLink);
            }
        });

        neteng1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                myWebLink.setData(Uri.parse("https://firebasestorage.googleapis.com/v0/b/setas-18760.appspot.com/o/NET%2Fnust-engineering-sample-paper-01.pdf?alt=media&token=dc6d67d7-0b28-40c7-b03b-e9ee6be835a8"));
                startActivity(myWebLink);
            }
        });

        neteng2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                myWebLink.setData(Uri.parse("https://firebasestorage.googleapis.com/v0/b/setas-18760.appspot.com/o/NET%2Fnust-engineering-sample-paper-02.pdf?alt=media&token=999b6c15-4390-45b6-a544-1feb04d6b984"));
                startActivity(myWebLink);
            }
        });

        netmg1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                myWebLink.setData(Uri.parse("https://firebasestorage.googleapis.com/v0/b/setas-18760.appspot.com/o/NET%2Fnust-sample-test-01-for-management-sciences.pdf?alt=media&token=6521b096-fd20-42ad-ad93-febe955e676a"));
                startActivity(myWebLink);
            }
        });

        netmg2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                myWebLink.setData(Uri.parse("https://firebasestorage.googleapis.com/v0/b/setas-18760.appspot.com/o/NET%2Fnust-sample-test-02-for-management-sciences.pdf?alt=media&token=18326398-ffc6-4a66-93f7-b9e72a6e9457"));
                startActivity(myWebLink);
            }
        });

        ibacs1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                myWebLink.setData(Uri.parse("https://firebasestorage.googleapis.com/v0/b/setas-18760.appspot.com/o/IBA%2FCS.pdf?alt=media&token=beb26557-c3cc-4678-93fe-a8418d7a634c"));
                startActivity(myWebLink);
            }
        });

        ibacs2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                myWebLink.setData(Uri.parse("https://firebasestorage.googleapis.com/v0/b/setas-18760.appspot.com/o/IBA%2FCS%202.pdf?alt=media&token=02363b87-b13f-4bcc-a0f2-a57e631da7ef"));
                startActivity(myWebLink);
            }
        });

        ibacs3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                myWebLink.setData(Uri.parse("https://firebasestorage.googleapis.com/v0/b/setas-18760.appspot.com/o/IBA%2FCS%203.pdf?alt=media&token=fc5c6df5-c403-43ea-8081-167c6b8d298d"));
                startActivity(myWebLink);
            }
        });

        ibabba1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                myWebLink.setData(Uri.parse("https://firebasestorage.googleapis.com/v0/b/setas-18760.appspot.com/o/IBA%2FBBA.pdf?alt=media&token=a9313235-4596-422c-b690-a04504c998ef"));
                startActivity(myWebLink);
            }
        });

        ibabba2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                myWebLink.setData(Uri.parse("https://firebasestorage.googleapis.com/v0/b/setas-18760.appspot.com/o/IBA%2FBBA%202.pdf?alt=media&token=35b9bc68-9f8e-46f4-b835-fe0280e53d6e"));
                startActivity(myWebLink);
            }
        });

        giki1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                myWebLink.setData(Uri.parse("https://firebasestorage.googleapis.com/v0/b/setas-18760.appspot.com/o/GIKI%2Fgiki-engineering-sample-paper-01.pdf?alt=media&token=06bddfdb-15d4-4eb2-ae58-be378c31c797"));
                startActivity(myWebLink);
            }
        });

        giki2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                myWebLink.setData(Uri.parse("https://firebasestorage.googleapis.com/v0/b/setas-18760.appspot.com/o/GIKI%2Fgiki-engineering-sample-paper-02.pdf?alt=media&token=6ff8081e-3f1b-408d-a240-a60ecd35c553"));
                startActivity(myWebLink);
            }
        });

        giki3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                myWebLink.setData(Uri.parse("https://firebasestorage.googleapis.com/v0/b/setas-18760.appspot.com/o/GIKI%2Fgiki-engineering-sample-paper-03.pdf?alt=media&token=36a0b9f0-2877-439f-9025-6f58b35102d1"));
                startActivity(myWebLink);
            }
        });

        giki4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                myWebLink.setData(Uri.parse("https://firebasestorage.googleapis.com/v0/b/setas-18760.appspot.com/o/GIKI%2Fgiki-engineering-sample-paper-04.pdf?alt=media&token=a4bb2a03-0a24-4dbc-a75e-1617134e76a2"));
                startActivity(myWebLink);
            }
        });

        giki5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                myWebLink.setData(Uri.parse("https://firebasestorage.googleapis.com/v0/b/setas-18760.appspot.com/o/GIKI%2Fgiki-engineering-sample-paper-05.pdf?alt=media&token=57b4fbdf-6694-46cf-9875-65bc2921806c"));
                startActivity(myWebLink);
            }
        });

    }
}