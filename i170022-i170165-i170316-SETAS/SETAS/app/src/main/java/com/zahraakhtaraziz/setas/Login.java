package com.zahraakhtaraziz.setas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;

public class Login extends AppCompatActivity {
    TextView signup,forgotPass;
    Button login;
    String stremail,strpassword;
    EditText email,password;
    FirebaseAuth mAuth;
    String flag="1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth=FirebaseAuth.getInstance();
        signup=findViewById(R.id.signup);
        forgotPass=findViewById(R.id.forgotpw);
        login=findViewById(R.id.login);
        email=findViewById(R.id.email);
        password=findViewById(R.id.password);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Login.this, Signup.class));

            }
        });

        forgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Login.this, ResetPassword.class));

            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.isEmpty(password.getText().toString()) ||  TextUtils.isEmpty(email.getText().toString()))
                {
                    Toast.makeText(Login.this,"Please enter complete information!",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    mAuth.signInWithEmailAndPassword(email.getText().toString(),password.getText().toString()).
                            addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if(task.isSuccessful()) {
                                        Toast.makeText(Login.this, "Logged in successfully!", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(Login.this, MainPage.class);
                                        intent.putExtra("flag",flag);
                                        startActivity(intent);
                                    }
                                    else if(!task.isSuccessful()){
                                        FirebaseAuthException e = (FirebaseAuthException )task.getException();
                                        Toast.makeText(Login.this, "Failed Registration: "+e.getMessage(), Toast.LENGTH_SHORT).show();
                                        Log.e("LoginActivity", "Failed Registration", e);
                                    }
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {

                                }
                            });
                }
                //startActivity(new Intent(Login.this, MainPage.class));
            }
        });

    }
}