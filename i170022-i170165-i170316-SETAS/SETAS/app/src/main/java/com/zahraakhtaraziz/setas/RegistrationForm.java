package com.zahraakhtaraziz.setas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegistrationForm extends AppCompatActivity {

    private Toolbar toolbar;

    FirebaseDatabase database;
    DatabaseReference reference;
    EditText fname,lname,cnic,province,city,phone,minstitution,iinstitution,mpercent,ipercent,mboard,iboard,program;
    //Spinner mspinner,ispinner;
    String strmboard,striboard,strfname,strlname,strcnic,strprovince,strcity,strphone,strmpercent,stripercent,strminstitution,striinstitution,strprogram;
    Button registerbtn;

//    Menu menu1;
//    MenuItem entrytest,manageprofil,searchuni,recommenduni,register,uni,account,settings,logout;
    ImageView home,profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_form);

        database = FirebaseDatabase.getInstance();
        reference = database.getReference("Users");

        fname=findViewById(R.id.fname);
        lname=findViewById(R.id.lname);
        cnic=findViewById(R.id.cnic);
        province=findViewById(R.id.province);
        city=findViewById(R.id.city);
        phone=findViewById(R.id.phone);
        mpercent=findViewById(R.id.mpercent);
        ipercent=findViewById(R.id.ipercent);
        mboard=findViewById(R.id.mboard);
        iboard=findViewById(R.id.iboard);
        minstitution=findViewById(R.id.minstitution);
        iinstitution=findViewById(R.id.iinstitution);
        program=findViewById(R.id.program);

        registerbtn=findViewById(R.id.register);
//        home=findViewById(R.id.home);
//        profile=findViewById(R.id.profile);

//        String[] boards=getResources().getStringArray(R.array.boards);
//        ArrayAdapter adapterMatric = new ArrayAdapter(this, android.R.layout.simple_spinner_item, boards);
//        adapterMatric.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        mspinner.setAdapter(adapterMatric);
//
//        ArrayAdapter adapterInter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, boards);
//        adapterInter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        ispinner.setAdapter(adapterInter);

        registerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strfname=fname.getText().toString();
                strlname=lname.getText().toString();
                strcnic=cnic.getText().toString();
                strprovince=province.getText().toString();
                strcity=city.getText().toString();
                strphone=phone.getText().toString();
                strminstitution=minstitution.getText().toString();
                strmpercent=mpercent.getText().toString();
                striinstitution=iinstitution.getText().toString();
                strprogram=program.getText().toString();
                stripercent=ipercent.getText().toString();
                strmboard=mboard.getText().toString();
                striboard=iboard.getText().toString();

               User myUser= new User(strfname,strlname,strcnic,strprovince,strcity,strphone,strminstitution,strmboard,strmpercent,striinstitution,striboard,stripercent,strprogram);
                reference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(myUser
                ).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Intent intent = new Intent(RegistrationForm.this, MainPage.class);
                            startActivity(intent);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(RegistrationForm.this, "Failed to store info: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

//        home.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(RegistrationForm.this, MainPage.class));
//            }
//        });
//        profile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(RegistrationForm.this, YourProfile.class));
//            }
//        });

    }

//    @Override
//    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//
//        System.out.println(adapterView.getId());
//        System.out.println(R.id.mspinner);
//        System.out.println(R.id.ispinner);
//
//        if(adapterView.getId() == R.id.mspinner)
//        {
//            strmboard=adapterView.getItemAtPosition(i).toString();
//        }
//        else if(adapterView.getId() == R.id.ispinner)
//        {
//           striboard=adapterView.getItemAtPosition(i).toString();
//        }
//    }
//
//    @Override
//    public void onNothingSelected(AdapterView<?> adapterView) {
//
//    }
}