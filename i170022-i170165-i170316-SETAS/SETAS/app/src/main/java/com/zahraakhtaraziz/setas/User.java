package com.zahraakhtaraziz.setas;

public class User {
    String fname,lname,cnic,province,city,phone,minstitution,mboard,mpercent,iinstitution,iboard,ipercent,program;

    public User()
    {}

    public User(String fname, String lname, String cnic, String province, String city, String phone, String minstitution, String mboard, String mpercent, String iinstitution, String iboard, String ipercent, String program) {
        this.fname = fname;
        this.lname = lname;
        this.cnic = cnic;
        this.province = province;
        this.city = city;
        this.phone = phone;
        this.minstitution = minstitution;
        this.mboard = mboard;
        this.mpercent = mpercent;
        this.iinstitution = iinstitution;
        this.iboard = iboard;
        this.ipercent = ipercent;
        this.program = program;
    }

    public String getMinstitution() {
        return minstitution;
    }

    public void setMinstitution(String minstitution) {
        this.minstitution = minstitution;
    }

    public String getIinstitution() {
        return iinstitution;
    }

    public void setIinstitution(String iinstitution) {
        this.iinstitution = iinstitution;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMboard() {
        return mboard;
    }

    public void setMboard(String mboard) {
        this.mboard = mboard;
    }

    public String getMpercent() {
        return mpercent;
    }

    public void setMpercent(String mpercent) {
        this.mpercent = mpercent;
    }

    public String getIboard() {
        return iboard;
    }

    public void setIboard(String iboard) {
        this.iboard = iboard;
    }

    public String getIpercent() {
        return ipercent;
    }

    public void setIpercent(String ipercent) {
        this.ipercent = ipercent;
    }
}
