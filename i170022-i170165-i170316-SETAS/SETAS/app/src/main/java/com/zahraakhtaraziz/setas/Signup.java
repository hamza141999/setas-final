package com.zahraakhtaraziz.setas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;

public class Signup extends AppCompatActivity {
    Button signup;
    TextView login;
    FirebaseAuth mAuth;
    EditText email,password,cpassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        email=findViewById(R.id.etName);
        password=findViewById(R.id.Password);
        cpassword=findViewById(R.id.Confirm_Password);
        mAuth=FirebaseAuth.getInstance();
        signup=findViewById(R.id.signup);
        login=findViewById(R.id.login);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Signup.this,MainActivity.class));
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!password.getText().toString().equals(cpassword.getText().toString()))
                {
                    Toast.makeText(Signup.this,"The passwords do not match! Please re-enter password!",Toast.LENGTH_SHORT).show();
                }

                else if(TextUtils.isEmpty(password.getText().toString()) || TextUtils.isEmpty(cpassword.getText().toString()) || TextUtils.isEmpty(email.getText().toString()))
                {
                    Toast.makeText(Signup.this,"Please enter complete information!",Toast.LENGTH_SHORT).show();
                }

                else {
                    mAuth.createUserWithEmailAndPassword(email.getText().toString(), password.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()) {
                                Intent intent = new Intent(Signup.this, RegistrationForm.class);
                                startActivity(intent);
                                Toast.makeText(Signup.this, "Successful Registration: ", Toast.LENGTH_SHORT).show();
                            }
                            else if(!task.isSuccessful()){
                                FirebaseException e = (FirebaseException )task.getException();
                                Toast.makeText(Signup.this, "Failed Registration: "+e.getMessage(), Toast.LENGTH_SHORT).show();
                                Log.e("LoginActivity", "Failed Registration", e);
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(Signup.this,"Failed to sign in: "+ e.getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser user=mAuth.getCurrentUser();
        if(user!=null)
        {
//            Toast.makeText(Signup.this,user.getUid()+"",Toast.LENGTH_SHORT).show();
            //Intent intent = new Intent(Register.this, Create_profile.class);
            //startActivity(intent);
        }
    }
}